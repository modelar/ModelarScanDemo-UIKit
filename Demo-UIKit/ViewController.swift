//
//  ViewController.swift
//  Demo-UIKit
//
//  Copyright Modelar Technologies 2022.
//

import UIKit
import ModelarScan 

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Authenticate the license (network connection required)
        // This only needs to be done once
        Authenticator.shared.authenticate(licenseKey: "LICENSE-KEY")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        // Create a scan controller
        let scanController = ScanController()
        
        // Configure real-time scanning
        scanController.scanResolution = 0.05
        scanController.scanSensorDensity = .high
        scanController.scanBackgroundTransparency = 0.3
        scanController.isPauseHidden = false
        
        // Configure automatic post-scanning refinement
        scanController.refinementQuality = .high
        
        // Configure model export settings
        scanController.upAxis = .z
        scanController.scanOrigin = .center
        
        // Add a handler that's called when a scan is accepted
        scanController.scanAcceptedHandler = { (acceptedScanPath: URL, acceptedSnapshot: UIImage) in
            
            // Print the path of the USDZ file
            print(acceptedScanPath)
            
            // Create a controller to share or save the file
            let objectsToShare = [acceptedScanPath]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.modalPresentationStyle = .automatic
            
            // Supply a view and frame for activity presentation controller to avoid
            // issues on iPads
            if let popOver = activityVC.popoverPresentationController {
                popOver.sourceView = scanController.view
                popOver.sourceRect = CGRect(x: scanController.view.bounds.width/2, y: scanController.view.bounds.height, width: 0, height: 0)
                popOver.permittedArrowDirections = .down
            }
            
            scanController.present(activityVC, animated:true)
        }
        
        // present the scan controller (must be full screen)
        scanController.modalPresentationStyle = .fullScreen
        self.present(scanController, animated: true)
    }


}

